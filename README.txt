Task lang
============

[] A todo task
[*] Ongoing task
[!] A task that's stopped, needs team attention
[?] Having doubts about task
[x] Completed task
[-] Canceled task

Subtasks
===========

[] This task has sub task

  [] This is a sub task


Comments
============

[] A task with comments

  This is first comment.

  ------------------------

  Comments can be multi line if they are big.
  Task lang supports that kind of comments.

  ------------------------

  This comment mentions @tina in it


  --------------------------

  We can link {/files/images/image.png} here.

  {/files/videos/video.webm}

  Possibly if a comment contains a line, nothing but a file in
  it, we can try to display it.

  ----------------------------

  A comment can contain code snippet

  ```ruby
  10.times{ puts "Hello World" }
  ```

  --------------

  This comment has inline code snippet `puts "Hello World"`.

  -------------

  There can be hyperlinks like https://fsf.org in comments too.

  -----------------------------

  Last comment, no need line after that :) Its optional.




People
================

[@kathik] This is a todo task and is assigned to @karthik

  To know about @karthik you can visit the file people/@karthik

[x @karthik] This is a task assigned to @karthik and is completed

[@karthik @ritu] This is a task assigned to @karthik and @ritu



Work done
===================

[] This is a task

  (1d 3h 15m @karthik) One day, three hours and fifteen minutes was spent
  by @karthik on this task.

  ----------------------

  (2019-04-05..2019-04-07 @karthik) @karthik has worked from 5th of April to
  7th of April on this task.

  ----------------------


  (2019-04-05:10:30..2019-04-05:13:30 @karthik) @karthik has worked from 10:30
  to 13:30hrs on this task

  ------------------------

  (4w 2d) This work has been done for 4 weeks two days

  ------------

  (1mo) This work was going on for 1 month

  ---------

  (2020-02-15) This work was done on 15th Feb 2020, and it has no time range
  specified



Estimates
=======================

[] This task is estimated / expected to be done on 2020-06-28 (2020-06-28)

[] This task is estimated to start on 19th may 2019 (2019-05-19..)

[] This task is estimated to end on 19th may 2019 (..2019-05-19)

[] This task has start and end date (2019-05-19..2019-05-21)

[] This task even has a time, upto the last second (2019-05-19:07:30:05..2019-05-21:07:45:59)

  This must be created by an MBA :D

[] This task is estimated in days, hours and minutes (4d 5h 3m)


Folder Structure
=============================

project_name/
  task_sheet_1.tasks
  task_sheet_2.tasks
  people/
    @person1.person
    @person2.person
  external_contacts/
    @company.company
    @contact.person
  notes/
    <timestamp>_notename.note
    @person1/
  mom/
    <timestamp>_momname.mom
  reminders/
    <utc time>-<reminder-name>.reminder
    @person1/
      <date>.reminder
  files/
    images/
    videos/
    docs/
  misc_work/
    @person1/
      <date>.work



.person file format
==================================

The file should be named @<user-name>.person

The file contains the following fields:

active: <yes / no>

name: <Name of the person>

utc offset: +5:30

image: {/files/images/@<username>.extension}

description:

  The description of the person. This could
  be a multi paragraph.

  One can write stories about himself.


links:
  <short one line description> | <web-site link>

contacts:

  phones:
    <primary phnum>
    <phnum 2>

  emails:
    <primary email>
    <email 2>

  addresses:

    primary address,
    just like a paragraph.

    ----------------

    secondary address




<date>.reminder file format (personal reminder)
==================================================

17:30 Name of the reminder

  Notes about the reminder

  ----------------------

  Second note about the reminder


<utc time>-<reminder-name>.reminder format (global reminder)
=================================================================

Reminder title in first line

Reminder description can be a big one.
That is as a paragraph.


------------------------

  Notes about the reminder.

  ----------------------------

  Another note about the reminder.



<date>.work file specification
==================================

This is a work log, look it has no time
accounted for work.

--------------------

(1h 15min) This is another work log, but it has
time specified in it.


<timestamp>_momname.mom
==============================

attendees:
  @person1
  @pweson2

notify:
  @person1
  @person4

Duration: 2019-11-21:12:00..2019-11-21:14:40

Agenda:

  * Agenda 1
  * Agenda 2

Minutes:

  This is a very long mom


Comments:

  Comment 1

  ------------------------

  Comment 2
